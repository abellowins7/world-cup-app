<?php

namespace Tests\Unit;

use App\Group;
use App\League;
use App\Player;
use App\Squad;
use App\Team;
use App\Venue;
use App\Tournament;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Symfony\Component\DomCrawler\Crawler;

class ExampleTest extends TestCase
{

    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function it_can_crawl_squads()
    {

        $players = file_get_contents(storage_path('app/data/players.json'));
        $cleaned = str_replace('\n', ' ', $players);
        $array = explode('\":', $cleaned);
        $newArray = [];
        unset($array[0]);
        foreach($array as $key => $value) {
            $value = str_replace(['\"', 'nextPlayer', '   ', ']', '['], '', trim($value));
            $newArray[$key] = explode(',', $value);
        }
        file_put_contents(storage_path('app/data/players.json'), json_encode($newArray));
        dd(json_encode($newArray));

        $client = new \Goutte\Client();
        $crawler = $client->request('GET', 'https://en.wikipedia.org/wiki/2019_Copa_Libertadores');
        $links = json_decode($this->teamWiki(), true);
        $players = [];

        foreach ($links as $key => $link) {
            $crawler = $client->request('GET', $link);
            $crawler->filter('.vcard.agent')->each(function ($node) use($key, &$players){
                $players[$key][] = $node->text() . ' nextPlayer ';
            });
        }

        dd(json_encode($players));



        $phantom = \JonnyW\PhantomJs\Client::getInstance();
        $phantom->getEngine()->setPath(base_path('bin/phantomjs'));
//        $phantom->isLazy();

        foreach ($remaining as $key => $link) {
            $team = Team::translatedName($key);
            $tournament = Tournament::where('name', 'Mundial Rusia 2018')->first();
            $team->tournaments()->attach($tournament->id);
            $request = $phantom->getMessageFactory()->createCaptureRequest($link, 'GET');
            $request->setDelay(5);
//            $request->setTimeout(5000);
            $response = $phantom->getMessageFactory()->createResponse();
            $phantom->send($request, $response);
            if ($response->getStatus() === 200) {
                $teamHtml = $response->getContent();
                $crawler = new Crawler();
                $crawler->addHtmlContent($teamHtml);
                $crawler->filter('.fi-team > .fi-team__members > .fi-p')->each(function ($node) use ($team) {
                    $imageUrl = null;
                    $playerHtml = $node->html();
                    $crawler = new Crawler();
                    $crawler->addHtmlContent($playerHtml);
                    $crawler->filter('.fi-p__picture > .fi-clip-svg')->each(function ($node) use (&$imageUrl) {
                        preg_match('/(?<=href=\").+(?=\")/', $node->html(), $match);
                        if (isset($match[0])) {
                            $imageUrl = explode('" ', $match[0]);
                            $imageUrl = $imageUrl[0];
                        }
                    });


                    $crawler->filter('.fi-p__info > .fi-p--link')->each(function ($node) use (&$jerseyNumber) {
                        $jerseyNumber = trim($node->text());
                    });

                    $crawler->filter('.fi-p__info > .fi-p__info--role')->each(function ($node) use (&$position) {
                        $position = trim($node->text());
                    });


                    $crawler->filter('.fi-p__info > .fi-p__info--age')->each(function ($node) use (&$age) {
                        $age = trim(str_replace('Age:', '', $node->text()));
                    });


                    $crawler->filter('.fi-p__info > .fi-p__n')->each(function ($node) use (&$player) {
                        //TODO link for more information
                        preg_match('/(?<=title=\").+(?=\")/', $node->html(), $match);
                        if (isset($match[0])) {
                            $player = ucwords(strtolower($match[0]));
                        }
                    });

                    $playerModel = Player::create([
                        'name' => $player,
                        'age' => $age,
                        'position' => $position,
                    ]);

                    $squad = Squad::create([
                        'team_id' => $team->id,
                        'player_id' => $playerModel->id,
                        'position' => $position,
                        'number' => intval($jerseyNumber)
                    ]);

                    $finalImageUrl = storage_path("players/{$playerModel->id}.jpg");
                    \Intervention\Image\Facades\Image::make($imageUrl)->save($finalImageUrl);
                    $playerModel->photo = $finalImageUrl;
                    $playerModel->save();

                });
            }
        }
    }

    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function it_can_crawl_wiki_squads()
    {
        $client = new \Goutte\Client();
        $links = [];
        $crawler = $client->request('GET', 'https://en.wikipedia.org/wiki/Gr%C3%AAmio_Foot-Ball_Porto_Alegrense#First_team_squad');
        dd($crawler);
        $crawler->filter('.fi-teams-list > .col-sm-4 > .fi-team-card')->each(function ($node) use (&$links) {
            $link = 'https://www.fifa.com' . $node->attr('href');
            $text = trim($node->text());
            $links[$text] = $link;
        });
        $remaining = array_slice($links, 13);
        $remaining = array_slice($remaining, 15);

        $phantom = \JonnyW\PhantomJs\Client::getInstance();
        $phantom->getEngine()->setPath(base_path('bin/phantomjs'));
//        $phantom->isLazy();
        foreach ($remaining as $key => $link) {
            $team = Team::translatedName($key);
            $tournament = Tournament::where('name', 'Mundial Rusia 2018')->first();
            $team->tournaments()->attach($tournament->id);
            $request = $phantom->getMessageFactory()->createCaptureRequest($link, 'GET');
            $request->setDelay(5);
//            $request->setTimeout(5000);
            $response = $phantom->getMessageFactory()->createResponse();
            $phantom->send($request, $response);
            if ($response->getStatus() === 200) {
                $teamHtml = $response->getContent();
                $crawler = new Crawler();
                $crawler->addHtmlContent($teamHtml);
                $crawler->filter('.fi-team > .fi-team__members > .fi-p')->each(function ($node) use ($team) {
                    $imageUrl = null;
                    $playerHtml = $node->html();
                    $crawler = new Crawler();
                    $crawler->addHtmlContent($playerHtml);
                    $crawler->filter('.fi-p__picture > .fi-clip-svg')->each(function ($node) use (&$imageUrl) {
                        preg_match('/(?<=href=\").+(?=\")/', $node->html(), $match);
                        if (isset($match[0])) {
                            $imageUrl = explode('" ', $match[0]);
                            $imageUrl = $imageUrl[0];
                        }
                    });


                    $crawler->filter('.fi-p__info > .fi-p--link')->each(function ($node) use (&$jerseyNumber) {
                        $jerseyNumber = trim($node->text());
                    });

                    $crawler->filter('.fi-p__info > .fi-p__info--role')->each(function ($node) use (&$position) {
                        $position = trim($node->text());
                    });


                    $crawler->filter('.fi-p__info > .fi-p__info--age')->each(function ($node) use (&$age) {
                        $age = trim(str_replace('Age:', '', $node->text()));
                    });


                    $crawler->filter('.fi-p__info > .fi-p__n')->each(function ($node) use (&$player) {
                        //TODO link for more information
                        preg_match('/(?<=title=\").+(?=\")/', $node->html(), $match);
                        if (isset($match[0])) {
                            $player = ucwords(strtolower($match[0]));
                        }
                    });

                    $playerModel = Player::create([
                        'name' => $player,
                        'age' => $age,
                        'position' => $position,
                    ]);

                    $squad = Squad::create([
                        'team_id' => $team->id,
                        'player_id' => $playerModel->id,
                        'position' => $position,
                        'number' => intval($jerseyNumber)
                    ]);

                    $finalImageUrl = storage_path("players/{$playerModel->id}.jpg");
                    \Intervention\Image\Facades\Image::make($imageUrl)->save($finalImageUrl);
                    $playerModel->photo = $finalImageUrl;
                    $playerModel->save();

                });
            }
        }
    }


    protected function teams()
    {
        return [
            'russia'
        ];
    }

    public function data(){

        return '
        \n
2\n
\n
\n
\n
DF\n
\n
Léo Moura';
    }
}
